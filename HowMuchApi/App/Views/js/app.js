/// <reference path="../html/main.html" />
/// <reference path="../html/main.html" />
(function (angular) {
    'use strict';

    angular.module('app', [
        'ngAnimate',
        'ngAria',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'dataCache'
    ]).config([
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider.when('/', {
                templateUrl: '/App/Views/html/main.html',
                controller: 'MainCtrl',
                caseInsensitiveMatch: true
            }).when('/Results', {
                templateUrl: '/App/Views/html/results.html',
                controller: 'ResultsCtrl',
                caseInsensitiveMatch: true
            }).otherwise({
                redirectTo: '/'
            });
        }
    ]).run([
        '$rootScope',
        function ($rootScope) {
            $rootScope.$on('$routeChangeStart', function (event, current) {
                if (current.$$route && current.$$route.resolve) {
                    // Show loading
                }
            });
            $rootScope.$on('$routeChangeSuccess', function () {
                // Hide loading
            });

            $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
                // Get message from rejection
                // Show errors
                // Hide loading
            });
        }
    ]).controller('AppCtrl', ['$scope', function ($scope) {
        var result = {};

        $scope.result = result;
        $scope.setResult = function (data) {
            angular.copy(data, result);
        };
    }]);

})(angular);