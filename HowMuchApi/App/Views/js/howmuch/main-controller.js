﻿(function (angular) {
    'use strict';

    angular.module('app').controller('MainCtrl', [
        '$scope', '$location','$window', 'preloadedData', 'mainFactory',
        function ($scope, $location, $window, preloadedData, mainFactory) {
            var pages = ['Landing', 'Age', 'Location', 'Profession', 'Recreation'],
                page = {
                    pageIndex: 0,
                    currentPage: 'Landing'
                },
                model = {};

            $scope.model = model;
            $scope.page = page;

            $scope.preloadedData = preloadedData;

            $scope.next = function (form) {
                var nextIndex = page.pageIndex + 1;
                if (form.$invalid) {
                    $window.alert('Please input a valid value');
                    return;
                }

                if (nextIndex < pages.length) {
                    page.pageIndex = nextIndex;
                    page.currentPage = pages[nextIndex];
                    return;
                }

                // Show loading
                mainFactory.calculate(model).then(function (result) {
                    $scope.setResult(result);
                    $location.path('/results/');
                }).catch(function (error) {
                    // Debug.write(error);
                    // Show error
                    $window.alert('Error\nOops, sorry about that, we seem to have misplaced a few processors.')
                }).finally(function () {
                    // Hide loading
                });
            };

            $scope.prev = function () {
                var prevIndex = page.pageIndex - 1;

                if (prevIndex < 0) {
                    prevIndex = 0;
                }

                page.pageIndex = prevIndex;
                page.currentPage = pages[prevIndex];
            };

        }]);
})(angular);