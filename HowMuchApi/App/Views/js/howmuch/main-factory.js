﻿(function (angular) {
    'use strict';

    angular.module('app').factory('mainFactory', ['$resource', function ($resource) {
        var resource = $resource('/api/EarningDeterminant/:id', { id: '@id' }, {
        });

        return {
            get: function (id) {
                return resource.get({ id: id }).$promise;
            },
            calculate: function (model) {
                return resource.save(model).$promise;
            }
        };
    }]);
})(angular);