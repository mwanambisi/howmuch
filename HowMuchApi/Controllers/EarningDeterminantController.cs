﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

using HowMuchApi.Data.Context;
using HowMuchApi.Models;
using HowMuchApi.Domain;

namespace HowMuchApi.Controllers
{
    public class EarningDeterminantController : ApiController
    {
        HowMuchContext _dbContext = new HowMuchContext();
        
        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public object Post([FromBody]CalcEarningModel model)
        {
            var age = _dbContext.Query<Age>()
                .Where(x => x.MinTier <= model.Age)
                .Where(x => x.MaxTier >= model.Age)
                .Select(x => new { x.Id, x.BaseSalary })
                .Single();

            //get earning amount, for now it is a simple addition
            var professionEarning = _dbContext.Query<Profession>()
                .Include(x => x.ProfessionType)
                .Where(x => x.Id == model.ProfessionId)
                .Select(x => x.ProfessionType.BaseSalary)
                .Single();
            var residenceEarning = _dbContext.Query<Residence>()
                .Include(x => x.Zone)
                .Where(x => x.Id == model.ResidenceId)
                .Select(x => x.Zone.BaseSalary)
                .Single();
            var recreationEarning = _dbContext.Query<Recreation>()
                .Where(x => x.Id == model.RecreationId)
                .Select(x => x.BaseSalary)
                .Single();
            var totalEarning = age.BaseSalary + professionEarning + residenceEarning + recreationEarning;
            
            //get humourtext
            var humourText = _dbContext.Query<HumourMapping>()
                .Where(x => x.ProfessionId == model.ProfessionId)
                .Where(x => x.ResidenceId == model.ResidenceId)
                .Where(x => x.RecreationId == model.RecreationId)
                .Where(x => x.AgeId == age.Id)
                .Select(x => x.Humour)
                .FirstOrDefault();

            var result = new EarningResultModel
            {
                EstimatedEarning = totalEarning,
                Humour = humourText ?? string.Empty
            };
            return result;

        }

    }
}