﻿using HowMuchApi.Data.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HowMuchApi.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            using (var db = new HowMuchContext())
            {
                var profession = db.Professions.Select(p => new { Id = p.Id, Name = p.Name }).ToList();
                var recreations = db.Recreations.Select(r => new { Id = r.Id, Name = r.Name }).ToList();
                var residences = db.Residences.Select(r => new { Id = r.Id, Name = r.Name }).ToList();

                var preloadedData = new
                {
                    Professions = profession,
                    Recreations = recreations,
                    Residences = residences
                };

                var settings = new JsonSerializerSettings();
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                ViewBag.PreloadedData = JsonConvert.SerializeObject(preloadedData, settings);
            }

            return View();
        }
    }
}