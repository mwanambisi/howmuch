﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HowMuchApi.Data.Context;
using HowMuchApi.Models;
using HowMuchApi.Domain;

namespace HowMuchApi.Controllers
{
    public class ResidenceController : ApiController
    {
        HowMuchContext _dbContext = new HowMuchContext();

        // GET api/<controller>
        public IEnumerable<SelectListItemModel> Get()
        {
            var data = _dbContext.Query<Residence>()
                .Select(x => new SelectListItemModel() { Id = x.Id, Value = x.Name })
                .ToList();
            
            return data;
        }
        
        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

    }
}