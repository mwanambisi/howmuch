﻿using System.Collections.Generic;
using System.Linq;

namespace HowMuchApi.Data.Context
{
    public static class DbInitializerHelper
    {
        public static List<string> GetResidenceNames()
        {
            const string names =
                @"Athi River, Bomet, Embu, Gikomba, Hurlingam, Kangemi, Karen, Kibera, Kileleshwa, Kilimani,
                Kisumu, Lamu, Langata, Lavington, Loresho, Malindi, Mathare, Mombasa, Muthaiga, Nairobi West, New Muthaiga, 
                Nyeri, Other, Runda, South B, South C, Spring Valley, Thika, Voi, Westlands";
            var residenceNames = names.Split(',').Select(n => n.Trim(new[] { ' ', '\r', '\n' })).ToList();
            return residenceNames;
        }

        public static List<string> GetProffesionNames()
        {
            const string names =
                @"Accountant, Architect, Casual Laborer, Doctor, Executive, Farmer, Finance Manager, Lawyer, Marketer,
                Nurse, Office Assistant, Programmer, Side Hustle, Student, Teacher, Unemployed";
            var professionNames = names.Split(',').Select(n => n.Trim(new[] { ' ', '\r', '\n' })).ToList();
            return professionNames;
        }

        public static List<string> GetFunActivityNames()
        {
            const string names =
                @"Pubs and more pubs, Golf at country club, Rugby, Spend time with family, Travel around kenya, Travel internationally, 
                   Photography, Sowing, Foodie, Social Media, TV junkie, Movie Monkey, Internet Troll, Race Cars";
            
            var activityNames = names.Split(',').Select(n => n.Trim(new[] {' ', '\r', '\n'})).ToList();
            return activityNames;
        }
    }
}