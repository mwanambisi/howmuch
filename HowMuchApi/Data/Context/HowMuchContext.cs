﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HowMuchApi.Domain;

namespace HowMuchApi.Data.Context
{
    public class HowMuchContext : DbContext
    {
        public HowMuchContext()
            : base("HowMuchContext")
        {
            
        }
        public DbSet<Age> Ages { get; set; }
        public DbSet<Profession> Professions { get; set; }
        public DbSet<Recreation> Recreations { get; set; }  
        public DbSet<ProfessionType> ProfessionTypes { get; set; }
        public DbSet<Residence> Residences { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<HumourMapping> HumourMappings { get; set; }

        public IQueryable<T> Query<T>()
            where T : class
        {
            return this.Set<T>().AsNoTracking();
        }

    }
}