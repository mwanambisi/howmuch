﻿namespace HowMuchApi.Domain
{
    public class Age
    {
        public Age()
        {
            
        }

        public int Id { get; set; }

        public int MinTier { get; set; }

        public int MaxTier { get; set; }

        public decimal BaseSalary { get; set; }

        public Age(int minTier, int maxTier, decimal baseSalary)
        {
            this.MinTier = minTier;
            this.MaxTier = maxTier;
            this.BaseSalary = baseSalary;
        }
    }
}
