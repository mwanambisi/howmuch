﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HowMuchApi.Domain
{
    public class HumourMapping
    {
        public int Id { get; private set; }
        public int? AgeId { get; private set; }
        public int? ProfessionId { get; private set; }
        public int? ResidenceId { get; private set; }
        public int? RecreationId { get; private set; }
        public string Humour { get; private set; }

        public HumourMapping()
        {
        }

        public HumourMapping(int? ageId, int? professionId, int? residenceId, int? recreationId,
            string humour)
        {
            this.AgeId = ageId;
            this.ProfessionId = professionId;
            this.ResidenceId = residenceId;
            this.RecreationId = recreationId;
            this.Humour = humour;
        }
    }
}