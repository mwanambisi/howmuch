﻿using System.ComponentModel.DataAnnotations;

namespace HowMuchApi.Domain
{
    public class Profession
    {
        public int Id { get; private set; }
        [StringLength(50)]
        public string Name { get; set; }
        public int ProfessionTypeId { get; set; }

        public virtual ProfessionType ProfessionType { get; set; }

        public Profession()
        {
        }

        public Profession(string name, int professionTypeId)
        {
            this.Name = name;
            this.ProfessionTypeId = professionTypeId;
        }
    }
}