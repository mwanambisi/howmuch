﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HowMuchApi.Domain
{
    public class ProfessionType
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public decimal BaseSalary { get; set; }

        public ProfessionType()
        {
        }

        public ProfessionType(string name, decimal baseSalary)
        {
            this.Name = name;
            this.BaseSalary = baseSalary;
        }
    }
}