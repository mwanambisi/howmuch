﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HowMuchApi.Domain
{
    public enum FunCategory : int
    {
        Cheap = 0,
        Moderate = 1,
        Expensive = 2,
        Lavish = 3
    }

    public class Recreation
    {
        public int Id { get; private set; }
        [StringLength(50)]
        public string Name { get; set; }
        public FunCategory FunCategory { get; set; }
        public decimal BaseSalary { get; set; }

        public Recreation()
        {
        }

        public Recreation(FunCategory funCategory, decimal baseSalary)
        {
            FunCategory = funCategory;
            BaseSalary = baseSalary;
        }
    }
}