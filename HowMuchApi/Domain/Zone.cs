﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HowMuchApi.Domain
{
    public class Zone
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Tag { get; set; }
        public decimal BaseSalary { get; set; }

        public Zone()
        {

        }

        public Zone(string tag, decimal baseSalary)
        {
            this.Tag = tag;
            this.BaseSalary = baseSalary;
        }
    }
}