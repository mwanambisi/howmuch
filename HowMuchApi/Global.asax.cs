using System.Data.Entity.Migrations;
using HowMuchApi.Migrations;

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace HowMuchApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Database.SetInitializer<OptiPluzContext>(new DropCreateDatabaseIfModelChanges<OptiPluzContext>());

             var migrator = new DbMigrator(new HowMuchApi.Migrations.Configuration());
            migrator.Update();
        }
    }
}
