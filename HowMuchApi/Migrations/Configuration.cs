using System;
using System.Collections.Generic;
using System.Linq;
using HowMuchApi.Data.Context;
using HowMuchApi.Domain;

namespace HowMuchApi.Migrations
{
    using System.Data.Entity.Migrations;

    public class Configuration : DbMigrationsConfiguration<HowMuchContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(HowMuchContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            AddAges(context);
            AddRecretions(context);
            AddProfessionTypes(context);
            AddZones(context);
            context.SaveChanges();
            AddResidences(context);
            AddProfessions(context);
            AddResultMappings(context);
        }

        private static void AddAges(HowMuchContext context)
        {
            context.Ages.AddOrUpdate(
                n => new {n.MinTier, n.MaxTier},
                new Age {MinTier = 15, MaxTier = 20, BaseSalary = 1000},
                new Age {MinTier = 21, MaxTier = 30, BaseSalary = 5000},
                new Age {MinTier = 31, MaxTier = 40, BaseSalary = 10000},
                new Age {MinTier = 41, MaxTier = 50, BaseSalary = 15000},
                new Age {MinTier = 51, MaxTier = 60, BaseSalary = 20000},
                new Age {MinTier = 61, MaxTier = 70, BaseSalary = 25000},
                new Age {MinTier = 71, MaxTier = 80, BaseSalary = 30000}
                );
        }

        private void AddProfessionTypes(HowMuchContext context)
        {
            context.ProfessionTypes.AddOrUpdate(
                n => n.Name,
                new ProfessionType
                    {
                        Name = "1",
                        BaseSalary = 300000
                    },
                new ProfessionType
                    {
                        Name = "2",
                        BaseSalary = 150000
                    },
                new ProfessionType
                    {
                        Name = "3",
                        BaseSalary = 110000
                    },
                new ProfessionType
                    {
                        Name = "4",
                        BaseSalary = 80000
                    },
                new ProfessionType
                    {
                        Name = "5",
                        BaseSalary = 55000
                    },
                new ProfessionType
                    {
                        Name = "6",
                        BaseSalary = 35000
                    },
                new ProfessionType
                    {
                        Name = "7",
                        BaseSalary = 15000
                    },
                new ProfessionType
                    {
                        Name = "8",
                        BaseSalary = 8000
                    },
                new ProfessionType
                    {
                        Name = "9",
                        BaseSalary = 10000
                    },
                new ProfessionType
                    {
                        Name = "10",
                        BaseSalary = 15000
                    });
        }
        
        private void AddZones(HowMuchContext context)
        {
            context.Zones.AddOrUpdate(n => n.Tag,
                                      new Zone {Tag = "A", BaseSalary = 350000},
                                      new Zone {Tag = "B", BaseSalary = 125000},
                                      new Zone {Tag = "C", BaseSalary = 4000},
                                      new Zone {Tag = "D", BaseSalary = 25000},
                                      new Zone {Tag = "E", BaseSalary = 5000}
                );
        }

        private void AddRecretions(HowMuchContext context)
        {
            context.Recreations.AddOrUpdate(
                n => n.Name,
                new Recreation
                    {
                        Name = "Pubs & more pubs",
                        FunCategory = FunCategory.Expensive
                    },
                new Recreation
                    {
                        Name = "Golf at Country Club",
                        FunCategory = FunCategory.Lavish
                    },
                new Recreation
                    {
                        Name = "Rugby",
                        FunCategory = FunCategory.Cheap
                    },
                new Recreation
                    {
                        Name = "Spend time with family",
                        FunCategory = FunCategory.Moderate
                    },
                new Recreation
                    {
                        Name = "Travel around Kenya",
                        FunCategory = FunCategory.Moderate
                    },
                new Recreation
                    {
                        Name = "Travel Internationally",
                        FunCategory = FunCategory.Expensive
                    },
                new Recreation
                    {
                        Name = "Sowing",
                        FunCategory = FunCategory.Cheap
                    },
                new Recreation
                    {
                        Name = "Foodie",
                        FunCategory = FunCategory.Moderate
                    },
                new Recreation
                    {
                        Name = "Social Media",
                        FunCategory = FunCategory.Cheap
                    },
                new Recreation
                    {
                        Name = "TV Junkie",
                        FunCategory = FunCategory.Cheap
                    },
                new Recreation
                    {
                        Name = "Movie Monkey",
                        FunCategory = FunCategory.Cheap
                    },
                new Recreation
                    {
                        Name = "Internet Troll"
                    },
                new Recreation
                    {
                        Name = "Race Cars",
                        FunCategory = FunCategory.Lavish
                    });
        }

        private void AddProfessions(HowMuchContext context)
        {
            context.Professions.AddOrUpdate(
                n => n.Name,
                new Profession
                    {
                        Name = "Accountant",
                        ProfessionTypeId = GetProffesionType("4", context).Id
                    },
                new Profession
                    {
                        Name = "Architect",
                        ProfessionTypeId = GetProffesionType("1", context).Id
                    },
                new Profession
                    {
                        Name = "Casual Labourer",
                        ProfessionTypeId = GetProffesionType("8", context).Id
                    },
                new Profession
                    {
                        Name = "Doctor",
                        ProfessionTypeId = GetProffesionType("1", context).Id
                    },
                new Profession
                    {
                        Name = "Executive",
                        ProfessionTypeId = GetProffesionType("1", context).Id
                    },
                new Profession
                    {
                        Name = "Farmer",
                        ProfessionTypeId = GetProffesionType("5", context).Id
                    },
                new Profession
                    {
                        Name = "Finance Manager",
                        ProfessionTypeId = GetProffesionType("3", context).Id
                    },
                new Profession
                    {
                        Name = "Lawyer",
                        ProfessionTypeId = GetProffesionType("2", context).Id
                    },
                new Profession
                    {
                        Name = "Marketer",
                        ProfessionTypeId = GetProffesionType("4", context).Id
                    },
                new Profession
                    {
                        Name = "Nurse",
                        ProfessionTypeId = GetProffesionType("6", context).Id
                    },
                new Profession
                    {
                        Name = "Office Assistant",
                        ProfessionTypeId = GetProffesionType("6", context).Id
                    },
                new Profession
                    {
                        Name = "Programmer",
                        ProfessionTypeId = GetProffesionType("3", context).Id
                    },
                new Profession
                    {
                        Name = "Side Hustle",
                        ProfessionTypeId = GetProffesionType("6", context).Id
                    },
                new Profession
                    {
                        Name = "Student",
                        ProfessionTypeId = GetProffesionType("9", context).Id
                    },
                new Profession
                    {
                        Name = "Teacher",
                        ProfessionTypeId = GetProffesionType("6", context).Id
                    },
                new Profession
                    {
                        Name = "Unemployed",
                        ProfessionTypeId = GetProffesionType("10", context).Id
                    });
        }

        private void AddResidences(HowMuchContext context)
        {
            context.Residences.AddOrUpdate(
                n => n.Name,
                new Residence
                    {
                        Name = "Athi River",
                        ZoneId = GetZone("B", context).Id
                    },
                new Residence
                    {
                        Name = "Bomet",
                        ZoneId = GetZone("D", context).Id
                    },
                new Residence
                    {
                        Name = "Buru Buru",
                        ZoneId = GetZone("B", context).Id
                    },
                new Residence
                    {
                        Name = "Gikomba",
                        ZoneId = GetZone("E", context).Id
                    },
                new Residence
                    {
                        Name = "Hurlingham",
                        ZoneId = GetZone("C", context).Id
                    },
                new Residence
                    {
                        Name = "Kangemi",
                        ZoneId = GetZone("E", context).Id
                    },
                new Residence
                    {
                        Name = "Karen",
                        ZoneId = GetZone("A", context).Id
                    },
                new Residence
                    {
                        Name = "Kibera",
                        ZoneId = GetZone("E", context).Id
                    },
                new Residence
                    {
                        Name = "Kileleshwa",
                        ZoneId = GetZone("C", context).Id
                    },
                new Residence
                    {
                        Name = "Kilimani",
                        ZoneId = GetZone("C", context).Id
                    },
                new Residence
                    {
                        Name = "Kisumu",
                        ZoneId = GetZone("D", context).Id
                    },
                new Residence
                    {
                        Name = "Lamu",
                        ZoneId = GetZone("E", context).Id
                    },
                new Residence
                    {
                        Name = "Langata",
                        ZoneId = GetZone("B", context).Id
                    },
                new Residence
                    {
                        Name = "Lavington",
                        ZoneId = GetZone("A", context).Id
                    },
                new Residence
                    {
                        Name = "Loresho",
                        ZoneId = GetZone("A", context).Id
                    },
                new Residence
                    {
                        Name = "Malindi",
                        ZoneId = GetZone("D", context).Id
                    },
                new Residence
                    {
                        Name = "Muthaiga",
                        ZoneId = GetZone("A", context).Id
                    },
                new Residence
                    {
                        Name = "Nairobi",
                        ZoneId = GetZone("C", context).Id
                    },
                new Residence
                    {
                        Name = "Nyeri",
                        ZoneId = GetZone("E", context).Id
                    },
                new Residence
                    {
                        Name = "Other",
                        ZoneId = GetZone("D", context).Id
                    },
                new Residence
                    {
                        Name = "Runda",
                        ZoneId = GetZone("A", context).Id
                    },
                new Residence
                    {
                        Name = "Thika",
                        ZoneId = GetZone("B", context).Id
                    },
                new Residence
                    {
                        Name = "Spring Valley",
                        ZoneId = GetZone("B", context).Id
                    },
                new Residence
                    {
                        Name = "Voi",
                        ZoneId = GetZone("B", context).Id
                    },
                new Residence
                    {
                        Name = "Westlands",
                        ZoneId = GetZone("B", context).Id
                    });
        }

        private void AddResultMappings(HowMuchContext context)
        {
            context.HumourMappings.AddOrUpdate(
                hm => hm.Humour,
                new HumourMapping(1, 14, 1, 2, "Manze! There are no golf parks in that place that allow kids!"),
                new HumourMapping(1, 4, 1, 1, "Kids are not allowed to drink!")
                );
        }

        private ProfessionType GetProffesionType(string str, HowMuchContext context)
        {
            return context.ProfessionTypes
                .FirstOrDefault(n => n.Name.Equals(str, StringComparison.InvariantCultureIgnoreCase));
        }

        private Zone GetZone(string str, HowMuchContext context)
        {
            return context.Zones
                .FirstOrDefault(n => n.Tag.Equals(str, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
