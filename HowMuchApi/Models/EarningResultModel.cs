﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HowMuchApi.Models
{
    public class EarningResultModel
    {
        public decimal EstimatedEarning { get; set; }
        public string Humour { get; set; }
    }
}