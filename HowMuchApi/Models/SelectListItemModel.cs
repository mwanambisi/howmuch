﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HowMuchApi.Models
{
    public class SelectListItemModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}